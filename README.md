Web-based light monitoring system


Display system load and system service requirements

Display CPU/GPU temperature sensor

Display the temperature and health status of the disk

Display statistics about network/port traffic and netstat

Show statistics about mail

Display statistics on the web server (Apache, Nginx and Lighttpd)

Show MySQL load and statistics

Show statistics about Squid proxy

Display statistics on NFS server/client

Display statistics on Raspberry Pi sensors

Show Memcached statistics
